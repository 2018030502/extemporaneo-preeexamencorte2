
import http from 'http';


import path from 'path';
import express from 'express'; // Import express using ES6 syntax
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';
import misRutas from './router/index.js';

const puerto = 8080;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();


// Configurar el motor de vistas EJS
app.set('view engine', 'ejs');

// Middleware para manejar datos URL-encoded
app.use(bodyParser.urlencoded({ extended: true }));

// Servir archivos estáticos desde el directorio 'public'
app.use(express.static(__dirname + '/public'));

// Iniciar el servidor
app.use(misRutas.router);
app.listen(puerto, () => {
    console.log("Escuchando en el puerto " + puerto);
});
