import express from 'express';

export const router = express.Router();

// Middleware para parsear el cuerpo de las peticiones
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get('/', (req, res) => {
    res.render('index', {titulo: "pre Examen pagina ejs", nombre: "Yamir Ezequiel Paez ayala"});
});

router.post('/salida', (req, res) => {
    // Desestructuración no necesaria aquí ya que vamos a calcular estos valores
    const { nombre, servicio, kilowatts } = req.body;

    // Lógica para determinar el costo por kilowatt basado en el tipo de servicio
    let costoKilowatt;
    switch (servicio) {
        case '1': costoKilowatt = 1.08; break;
        case '2': costoKilowatt = 2.5; break;
        case '3': costoKilowatt = 3.0; break;
        default: costoKilowatt = 0;
    }

    // Cálculos
    const subTotal = kilowatts * costoKilowatt;
    let descuento;
    if (kilowatts <= 1000) {
        descuento = subTotal * 0.10;
    } else if (kilowatts > 1000 && kilowatts <= 10000) {
        descuento = subTotal * 0.20;
    } else {
        descuento = subTotal * 0.50;
    }
    const impuesto = subTotal * 0.16;
    const total = subTotal + impuesto - descuento;

    // Envío de los datos calculados a la vista 'salida'
    res.render('salida', { 
        nombre, 
        tipoServicio: servicio, // Aquí deberías convertir el valor numérico a una cadena descriptiva antes de enviarlo.
        kilowatts, 
        costoK: costoKilowatt.toFixed(2), 
        subTotal: subTotal.toFixed(2), 
        impuesto: impuesto.toFixed(2), 
        descuento: descuento.toFixed(2), 
        total: total.toFixed(2) 
    });    
});


export default {router}